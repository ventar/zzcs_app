/**
 * Created by zhxfj on 2017/11/17.
 */
var questionsData = [{
  tip: '那一个选项最能贴切的描绘你最近一星期以内的感受或行为？',
  num: '4',
  questions: [
  {
    title: '头疼',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '神经过敏，心中不踏实',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '心中有不必要的想法或字句盘旋',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '头昏或昏倒',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }]
}, {
  tip: '那一个选项最能贴切的描绘你最近一星期以内的感受或行为？',
  num: '4',
  questions: [
  {
    title: '头疼',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '神经过敏，心中不踏实',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '心中有不必要的想法或字句盘旋',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '头昏或昏倒',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }]
}, {
  tip: '那一个选项最能贴切的描绘你最近一星期以内的感受或行为？',
  num: '4',
  questions: [
  {
    title: '头疼',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '神经过敏，心中不踏实',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '心中有不必要的想法或字句盘旋',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '头昏或昏倒',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }]
}, {
  tip: '那一个选项最能贴切的描绘你最近一星期以内的感受或行为？',
  num: '4',
  questions: [
  {
    title: '头疼',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '神经过敏，心中不踏实',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '心中有不必要的想法或字句盘旋',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }, {
    title: '头昏或昏倒',
    answer: [{
      content: '从无'
    }, {
      content: '轻度'
    }, {
      content: '中度'
    }, {
      content: '偏重'
    }, {
      content: '严重'
    }]
  }]
}];