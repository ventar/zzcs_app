/**
 * Created by wwb on 2017/6/28.
 */
(function (W, $, J, B) {
  if (typeof W.app != 'undefined') {
    return;
  }

  function app() {}

  app.prototype.origin = W.location.origin;
  /** 专题url */
  app.prototype.specialSubjectUrl = "http://zhuanti.chaoxing.com/mobile/mooc/tocourse/";
  /** 课程url */
  app.prototype.courseUrl = "http://mooc1-api.chaoxing.com/phone/courseindex?courseid=";
  /** 图书url */
  app.prototype.bookUrl = "/m/book/detail/";

  /** 页面刷新标识 */
  app.prototype.pageRefreshFlag = "page_refresh_flag";
  /** 数据删除标识 */
  app.prototype.dataDeleteFlag = "data_delete_flag";
  /** 数据修改标识 */
  app.prototype.dataModifyFlag = "data_modify_flag";
  /** 数据变更id */
  app.prototype.dataIdFlag = "data_id_flag";

  app.prototype.resCategory = {
    "specialSubject": { "id": "100000001", "name": "专题" },
    "course": { "id": "100000002", "name": "课程" },
    "magazine": { "id": "100000006", "name": "期刊" },
    "webUrl": { "id": "100000015", "name": "网页" }
  };
  app.prototype.browser = {
    versions: function () {
      var u = navigator.userAgent,
        app = navigator.appVersion;
      return { //移动终端浏览器版本信息
        trident: u.indexOf('Trident') > -1, //IE内核
        presto: u.indexOf('Presto') > -1, //opera内核
        webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
        gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
        mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
        ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
        android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
        iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
        iPad: u.indexOf('iPad') > -1, //是否iPad
        webApp: u.indexOf('Safari') == -1
        //是否web应该程序，没有头部与底部
      };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
  };
  /**
   * 判断当前环境是不是移动端
   * @returns {boolean|*}
   */
  app.prototype.isApp = function () {
    return this.browser.versions.mobile;
  };
  /**
   * 判断当前环境是不是安卓
   */
  app.prototype.isAndroid = function () {
    return this.browser.versions.android;
  };
  /**
   * 判断当前环境是不是ios
   */
  app.prototype.isIos = function () {
    return this.browser.versions.iPhone;
  };
  /**
   * 判断当前环境是不是微信
   */
  app.prototype.isWeixin = function () {
    if (this.browser.versions.mobile) { //判断是否是移动设备打开。browser代码在下面
      var ua = navigator.userAgent.toLowerCase(); //获取判断用的对象
      if (ua.match(/MicroMessenger/i) == 'micromessenger') {
        return true;
      }
      return false;
    } else {
      return false;
    }
  }
  /**
   * 字符串是否为空
   * @param str
   */
  app.prototype.isEmpty = function (str) {
    return typeof (str) == 'undefined' || str == null || "" === $.trim(str);
  };
  /**
   * 判断是否是超星app
   * @returns {boolean}
   */
  app.prototype.isChaoxingApp = function () {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.match(/ChaoXingStudy/i) == "chaoxingstudy") {
      return true;
    }
    return false;
  };
  /**
   * 通知页面刷新
   */
  app.prototype.noticeRefresh = function () {
    var that = this;
    if (that.isChaoxingApp()) {
      var storage = W.localStorage;
      storage[that.pageRefreshFlag] = "1";
      B.postNotification("CLIENT_REFRESH_STATUS", { "status": "1" });
    }
  }
  /**
   * 通知修改
   */
  app.prototype.noticeModify = function (id) {
    var that = this;
    if (that.isChaoxingApp()) {
      var storage = W.localStorage;
      storage[that.dataModifyFlag] = "1";
      storage[that.dataIdFlag] = id;
      B.postNotification("CLIENT_REFRESH_STATUS", { "status": "1" });
    }
  };
  /**
   * 通知删除
   */
  app.prototype.noticeDelete = function (id) {
    var that = this;
    if (that.isChaoxingApp()) {
      var storage = W.localStorage;
      storage[that.dataDeleteFlag] = "1";
      storage[that.dataIdFlag] = id;
      B.postNotification("CLIENT_REFRESH_STATUS", { "status": "1" });
    }
  };
  /**
   * 绑定刷新事件
   */
  app.prototype.bindRefresh = function (callback) {
    var cmd = "CLIENT_REFRESH_EVENT";
    B.bind(cmd, callback);
  };
  /**
   * 退出app
   * @param tips 退出时的提示信息
   */
  app.prototype.exit = function (tips) {
    if (this.isChaoxingApp()) {
      B.postNotification("CLIENT_EXIT_WEBAPP", {
        message: tips || ''
      });
    } else {
      this.noAppMsg();
    }
  };
  /**
   * 关闭当前页
   * @param tips
   */
  app.prototype.closeView = function (tips) {
    if (this.isChaoxingApp()) {
      B.postNotification("CLIENT_EXIT_LEVEL", {
        message: tips || ''
      });
    }
  };

  /**
   * 打开图片
   * @param img_urlArray
   * @param index
   */
  app.prototype.clientPreviewImages = function (img_urlArray, index) {
    if (this.isChaoxingApp()) {
      var cmd = 'CLIENT_PREVIEW_IMAGES';
      B.postNotification(cmd, {
        imageUrls: img_urlArray,
        showIndex: index
      });
    }
  };

  /**
   * 封装打开url方法
   * @param option
   */
  app.prototype.packageOpenUrl = function (option) {
    if ($.isEmptyObject(option)) {
      return;
    }
    if (/.*[\u4e00-\u9fa5]+.*$/.test(option.webUrl)) { //有中文进行编码
      option.webUrl = encodeURI(option.webUrl);
    }
    if (option.webUrl.indexOf('/') == 0) {
      option.webUrl = location.protocol + "//" + location.hostname + (location.port == 80 ? "" : (":" + location.port + "")) + option.webUrl;
    }
    B.postNotification("CLIENT_OPEN_URL", $.extend({
      title: '', //标题
      loadType: 1, //打开方式，0在本页面打开，1使用客户端webview打开新页面，2打开系统浏览器
      webUrl: '', //要打开的url
      toolbarType: 1
    }, option));
  }
  /**
   * 当前页面打开url
   * @param url
   * @param title
   */
  app.prototype.replaceUrl = function (url, title) {
    var option = {
      "title": title,
      "webUrl": url,
      "loadType": 0
    };
    if (this.isChaoxingApp()) {
      this.packageOpenUrl(option);
    } else {
      window.location.replace(url);
    }
  };
  /**
   * 打开url
   * @param url
   * @param title
   */
  app.prototype.openUrl = function (url, title) {
    var option = {
      "title": title,
      "webUrl": url,
      "loadType": 1
    };
    if (this.isChaoxingApp()) {
      this.packageOpenUrl(option);
    } else {
      window.location.href = url;
    }
  };
  /**
   *调用浏览器打开url
   * @param url
   * @param title
   */
  app.prototype.openUrlByBrower = function (url, title) {
    var option = {
      "title": title,
      "webUrl": url,
      "loadType": 2
    };
    if (this.isChaoxingApp()) {
      this.packageOpenUrl(option);
    } else {
      window.location.href = url;
    }
  };
  /**
   * 打开url但是不带顶部栏
   * @param url
   * @param title
   */
  app.prototype.openUrlNoBar = function (url, title) {
    var option = {
      "title": title,
      "webUrl": url,
      "loadType": 1,
      "toolbarType": 0
    };
    if (this.isChaoxingApp()) {
      this.packageOpenUrl(option);
    } else {
      window.location.href = url;
    }
  };

  /**
   * 打开url不要历史
   * @param url
   * @param title
   */
  app.prototype.openUrlWithNoHistory = function (url, title) {
    var option = {
      "title": title,
      "webUrl": url,
      "loadType": 1,
      "toolbarType": 0

    };
    if (this.isChaoxingApp()) {
      this.packageOpenUrl(option);
    } else {
      window.location.href = url;
    }
  };

  /**
   * 打开url 跳过历史
   * @param url
   * @param title
   */
  app.prototype.openUrlSkipHistory = function (url, title) {
    var option = {
      "title": title,
      "webUrl": url + "#inner",
      "loadType": 0
    };
    if (this.isChaoxingApp()) {
      this.packageOpenUrl(option);
    } else {
      window.location.href = url;
    }
  };
  /**
   * 返回上一页
   */
  app.prototype.goback = function () {
    if (this.isChaoxingApp()) {
      jsBridge.postNotification('CLIENT_EXIT_LEVEL', {});
    } else {
      history.back();
    }
  };

  /**
   * 根据uid打开用户信息页面
   * @param uid
   */
  app.prototype.openUserInfoPageByUid = function (uid) {
    B.postNotification('CLIENT_OPEN_USERINFO', {
      UserID: '' + uid + '',
      passportID: ''
    });
  }
  /**
   * 根据puid打开用户信息页面
   * @param puid
   */
  app.prototype.openUserInfoPageByPUid = function (puid) {
    B.postNotification('CLIENT_OPEN_USERINFO', {
      UserID: '',
      passportID: '' + puid + ''
    });
  };
  /**
   * 显示提示信息
   * @param msg
   */
  app.prototype.showMsg = function (msg) {
    if (this.isChaoxingApp()) {
      B.postNotification('CLIENT_DISPLAY_MESSAGE', {
        message: msg
      });
    } else {
      alert(msg);
    }
  };
  /**
   * 获取自己的用户信息
   * @param callback
   */
  app.prototype.getSelfUserInfo = function (callback) {
    if (this.isChaoxingApp()) {
      var cmd = 'CLIENT_GET_USERINFO';
      B.unbind(cmd);
      callback && B.bind(cmd, callback);
      B.postNotification(cmd, {
        accountKey: ""
      });
    } else {
      this.noAppMsg();
    }
  };
  /**
   * 打开专题
   * @param resId
   * @param resName
   */
  app.prototype.openSpecial = function (resId, resName) {
    var url = this.specialSubjectUrl + resId;
    this.openUrl(url, resName);
  };
  /**
   * 打开图书
   * @param resId
   * @param title
   */
  app.prototype.openBook = function (bookId, title) {
    var url = this.origin + ctx + this.bookUrl + bookId;
    this.openUrl(url, title);
  };

  /**
   * 打开课程
   * @param resId
   * @param resName
   * @param teacherfactor
   * @param bbsid
   */
  app.prototype.openCourse = function (resId, resName, imgUrl, teacherfactor, bbsid) {
    var that = this;
    if (that.isChaoxingApp()) {
      var opts = { "bbsid": bbsid, "course": { "data": [{ "id": resId, "imageurl": imgUrl, "name": resName, "teacherfactor": teacherfactor }] }, "id": resId };
      B.postNotification('CLIENT_OPEN_RES', { "cataid": that.resCategory.course.id, "key": resId, "content": opts, "cataName": that.resCategory.course.name });
    } else {
      var url = this.courseUrl + resId;
      that.openUrl(url, resName);
    }
  };
  /**
   *打开小组
   * @param groupId
   */
  app.prototype.openGroup = function (groupId) {
    if (this.isChaoxingApp()) {
      var cmd = 'CLIENT_OPEN_GROUP';
      B.postNotification(cmd, {
        "GroupId": groupId,
        "needRecord": "false"
      });
    } else {
      this.noAppMsg();
    }
  }
  /**
   *打开通知
   * @param
   */
  app.prototype.openNotice = function () {
    if (this.isChaoxingApp()) {
      var cmd = 'CLIENT_OPEN_SEND_NOTICE';
      B.postNotification(cmd, { "showType": 1 });
    } else {
      this.noAppMsg();
    }
  }
  // /打开话题/
  app.prototype.openCircle = function (GroupId) {
    if (this.isChaoxingApp()) {
      var cmd = 'CLIENT_OPEN_GROUP';
      B.postNotification(cmd, {
        GroupId: GroupId,
        needRecord: 'true'
      });
    } else {
      this.noAppMsg();
    }
  }
  /**
   *打开我的课程
   * @param
   */
  app.prototype.openMyCourse = function () {
    if (this.isChaoxingApp()) {
      var cmd = 'CLIENT_OPEN_MYLESSONRES';
      B.postNotification(cmd, {});
    } else {
      this.noAppMsg();
    }
  }
  /**
   * 打开书架
   */
  app.prototype.openBookshelf = function () {
    var content = {
      "accountKey": "",
      "aid": "2108",
      "appid": "tushu",
      "appname": "书架",
      "appurl": "",
      "available": "1",
      "bind": "1",
      "cataid": "0",
      "description": "",
      "extendField": "",
      "focus": "0",
      "id": "2108",
      "isPrivate": "1",
      "isWebapp": "0",
      "loginId": "0",
      "logopath": "http://img1.16q.cn/ab5a8d921470c687c19e380a6a395905?w={WIDTH}&h={HEIGHT}",
      "needLogin": "0",
      "needRegist": "0",
      "norder": "15",
      "properties": "",
      "useClientTool": "1"
    };
    B.postNotification('CLIENT_OPEN_RES', { "cataid": 0, "key": "2108", "content": content, "cataName": "书架" });
  }
  /**
   * 订阅专题
   * @param resId
   * @param resName
   * @param imgUrl
   * @param teacherfactor
   * @param callback
   */
  app.prototype.subscribeSpecialSubject = function (resId, resName, imgUrl, teacherfactor, callback) {
    var that = this;
    var url = that.specialSubjectUrl + resId;
    if (that.isChaoxingApp()) {
      var options = {
        "cataid": that.resCategory.specialSubject.id,
        "cataName": that.resCategory.specialSubject.name,
        "key": "mooc_" + resId,
        "content": {
          "aid": "mooc_" + resId,
          "appid": $.md5(url),
          "appname": resName,
          "appurl": url,
          "cataid": that.resCategory.specialSubject.id,
          "logopath": imgUrl,
          "otherConfig": {
            "author": teacherfactor,
            "id": "mooc_" + resId
          }
        }
      };
      var cmd = "CLIENT_SUBSCRIBE_RES";
      B.unbind(cmd);
      callback && B.bind(cmd, callback);
      B.postNotification(cmd, $.extend({
        "cataid": that.resCategory.specialSubject.id,
        "cataName": that.resCategory.specialSubject.name,
        "key": "",
        "content": {
          "accountKey": "",
          "aid": "",
          "appid": "",
          "appname": "",
          "appurl": "",
          "available": 1,
          "bind": 1,
          "cataid": that.resCategory.specialSubject.id,
          "clientType": 127,
          "description": "",
          "focus": 0,
          "id": -1,
          "isPrivate": 1,
          "isWebapp": 1,
          "loginId": 0,
          "loginUrl": "",
          "logoPath": "",
          "logopath": "",
          "logoshowtype": 1,
          "needLogin": 0,
          "needRegist": 0,
          "norder": 2147483647,
          "otherConfig": {
            "author": "",
            "id": ""
          },
          "productId": 3,
          "properties": "",
          "rights": 1,
          "usable": "",
          "useClientTool": 2
        }
      }, options));
    } else {
      this.noAppMsg();
    }
  };
  /**
   * 订阅专题的回调
   * @param callback
   */
  /*app.prototype.subscribeSpecialSubjectCallBack = function(callback){
      var that = this;
      if(that.isChaoxingApp()){
          B.bind('CLIENT_SUBSCRIBE_RES', function(object) {
              if (!that.isEmpty(object)) {
                  eval("("+ callback + "("+ object +")" +")");
              }
          });
      }else{
          that.noAppMsg();
      }
  }*/
  /**
   * 取消专题订阅
   * @param resId
   * @param callback
   */
  app.prototype.unSubscribeSpecialSubject = function (resId, callback) {
    var that = this;
    if (that.isChaoxingApp()) {
      var cmd = "CLIENT_REMOVE_RES";
      B.unbind(cmd);
      callback && B.bind(cmd, callback);
      B.postNotification(cmd, {
        cataid: that.resCategory.specialSubject.id,
        cataName: that.resCategory.specialSubject.name,
        key: "mooc_" + resId
      });
    } else {
      that.noAppMsg();
    }
  };
  /**
   * 取消专题订阅的回调
   * @param callback
   */
  /*app.prototype.unSubscribeSpecialSubjectCallBack = function(callback){
      var that = this;
      if(that.isChaoxingApp()){
          B.bind('CLIENT_REMOVE_RES', function(object) {
              if (!that.isEmpty(object)) {
                  eval("("+ callback + "("+ object +")" +")");
              }
          });
      }else{
          that.noAppMsg();
      }
  }*/
  /**
   * 查询专题的订阅状态
   * @param resId
   * @param callback
   */
  app.prototype.getSpecialSubjectSubscribeStatus = function (resId, callback) {
    var that = this;
    if (that.isChaoxingApp()) {
      if (that.isEmpty(resId)) {
        return;
      }
      resId = resId + "";
      if (resId.indexOf("mooc_") == -1) {
        resId = "mooc_" + resId
      }
      var cmd = "CLIENT_RES_SUBSCRIPTION_STATUS";
      B.unbind(cmd);
      callback && B.bind(cmd, callback);
      B.postNotification(cmd, {
        "cataid": that.resCategory.specialSubject.id,
        "cataName": that.resCategory.specialSubject.name,
        "key": resId
      });
    } else {
      that.noAppMsg();
    }
  };
  app.prototype.subscribeCourse = function () {

  };
  app.prototype.unSubscribeCourse = function () {

  };
  app.prototype.getCourseSubscribeStatus = function () {

  };
  /**
   * 订阅期刊
   * @param resId
   * @param resName
   * @param imgUrl
   * @param url
   * @param teacherfactor
   * @param callback
   */
  app.prototype.subscribeMagazine = function (resId, resName, imgUrl, url, teacherfactor, callback) {
    var that = this;
    if (that.isChaoxingApp()) {
      var options = {
        "cataid": that.resCategory.magazine.id,
        "cataName": that.resCategory.magazine.name,
        "key": resId,
        "content": {
          "aid": "mooc_" + resId,
          "appid": $.md5(url),
          "appname": resName,
          "appurl": url,
          "cataid": that.resCategory.magazine.id,
          "logopath": imgUrl,
          "otherConfig": {
            "author": teacherfactor,
            "id": resId
          }
        }
      };
      var cmd = "CLIENT_SUBSCRIBE_RES";
      B.unbind(cmd);
      callback && B.bind(cmd, callback);
      B.postNotification(cmd, $.extend({
        "cataid": that.resCategory.magazine.id,
        "cataName": that.resCategory.magazine.name, //
        "key": "",
        "content": {
          "accountKey": "",
          "aid": "",
          "appid": "e02b6ac9eda31ca999980e82b02cbf84",
          "appname": "大学生必读书目",
          "appurl": "http://zhuanti.chaoxing.com/mobile/mooc/tocourse/81664193",
          "available": 1,
          "bind": 1,
          "cataid": that.resCategory.magazine.id,
          "clientType": 127,
          "description": "",
          "focus": 0,
          "id": -1,
          "isPrivate": 1,
          "isWebapp": 1,
          "loginId": 0,
          "loginUrl": "",
          "logoPath": "",
          "logopath": "http://p.ananas.chaoxing.com/star3/{WIDTH}_{HEIGHT}c/53cf1ebba310abc6bf23342b.jpg",
          "logoshowtype": 1,
          "needLogin": 0,
          "needRegist": 0,
          "norder": 2147483647,
          "otherConfig": {
            "author": "叶小鸣",
            "id": "mooc_81664193"
          },
          "productId": 3,
          "properties": "",
          "rights": 1,
          "usable": "",
          "res_src": "market",
          "useClientTool": 2
        }
      }, options));
    } else {
      this.noAppMsg();
    }
  };
  /**
   * 取消订阅期刊
   * @param resId
   * @param callback
   */
  app.prototype.unSubscribeMagazine = function (resId, callback) {
    var that = this;
    var cmd = "CLIENT_REMOVE_RES";
    B.unbind(cmd);
    callback && B.bind(cmd, callback);
    B.postNotification(cmd, {
      cataid: that.resCategory.magazine.id,
      key: resId
    });
  };
  /**
   * 查询期刊的订阅状态
   * @param resId
   * @param callback
   */
  app.prototype.getMagazineSubscribeStatus = function (resId, callback) {
    var that = this;
    var cmd = "CLIENT_RES_SUBSCRIPTION_STATUS";
    B.unbind(cmd);
    callback && B.bind(cmd, callback);
    if (that.isChaoxingApp()) {
      B.postNotification(cmd, {
        "cataid": that.resCategory.magazine.id,
        "appid": resId,
        "key": resId
      });
    } else {
      that.noAppMsg();
    }
  };
  /**
   * 获取订阅状态回调
   * @param callback
   */
  /*app.prototype.getSubscribeStatusCallBack = function(callback){
      var that = this;
      if(that.isChaoxingApp()){
          B.bind('CLIENT_RES_SUBSCRIPTION_STATUS', function(object) {
              if (!that.isEmpty(object)) {
                  eval("("+ callback + "("+ object +")" +")");
              }
          });
      }else{
          that.noAppMsg();
      }
  };*/
  /**
   * 非app提示信息
   * @param msg
   */
  app.prototype.noAppMsg = function (msg) {
    /*if(this.isEmpty(msg)){
        this.showMsg("当前不是app应用");
    }else{
        this.showMsg(msg);
    }*/
  };
  /**
   * ajax请求
   * @param url
   * @param params
   * @param successCallBack
   * @param errorCallBack
   */
  app.prototype.ajaxRequest = function (url, params, successCallBack, errorCallBack) {
    $.ajax({
      url: url,
      type: "post",
      data: params,
      dataType: "json",
      success: function (data) {
        successCallBack(data);
      },
      error: function () {
        errorCallBack();
      }
    });
  };
  /**
   * 获取json对象
   * @param jsonStr
   */
  app.prototype.getJsonObject = function (jsonStr) {
    if (this.isEmpty(jsonStr)) {
      return null;
    }
    return J.parse(jsonStr);
  };
  /**
   * 获取json字符串
   * @param jsonObject
   */
  app.prototype.getJsonStr = function (jsonObject) {
    if (this.isEmpty(jsonObject)) {
      return null;
    }
    return J.stringify(jsonObject);
  };
  /**
   * 获取请求参数
   * @param name
   * @returns {*}
   */
  app.prototype.getUrlParamter = function (name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
      return decodeURL(r[2]);
    }
    return null;
  }
  /**
   * 去测评
   */
  app.prototype.toEvaluate = function (resId, userMissionId) {
    var url = ctx + "/m/question/" + resId + "/" + userMissionId + "/to-answer";
    var opts = { "title": "", "checkLogin": "1", "loadType": "1", "webUrl": url, "toolbarType": "2" };
    this.packageOpenUrl(opts);
  }

  /**
   * 测评完成
   */
  app.prototype.completeEvaluate = function (url, title) {
    var opts = { "title": title, "checkLogin": "1", "loadType": "1", "webUrl": url + "#INNER", "toolbarType": "2" };
    B.postNotification("CLIENT_OPEN_URL", opts);
  }
  /**
   * 绑定回到顶部事件
   */
  app.prototype.bindScrollToTop = function (domId) {
    $(window).scroll(function () {
      if ($(document).scrollTop() > 200) {
        $("#" + domId).fadeIn(1000);
      } else {
        $("#" + domId).fadeOut(1000);
      }
    });
    $("#" + domId).click(function () {
      $('body,html').animate({ scrollTop: 0 }, 500);
      return false;
    });
  }
  /*
   * 转发网页
   */
  app.prototype.transmitWebUrl = function (title, url, logoUrl) {
    var that = this;
    var resUid = $.md5(url);
    var content = { "resTitle": title, "resUrl": url, "resLogo": logoUrl, "resUid": resUid, "toolbarType": 2 }
    B.postNotification("CLIENT_TRANSFER_INFO", { "cataid": that.resCategory.webUrl.id, "content": content });
  }

  /**
   * 打开图书
   *
   * @param option
   */
  app.prototype.openBookRes = function (option) {
    var cmd = 'CLIENT_READ_BOOK';

    B.postNotification(cmd, $.extend({
      type: 'pdz', // 图书类型， 默认是pdz
      uniqueID: '', // 唯一id, 这里是ssid
      remoteUrl: '', // 图书下载地址，book协议地址
      bookName: '', // 图书名
      message: '', // 信息
      coverUrl: '' // 封面地址
    }, option));
  };

  /**
   * 打开视频播放器
   *
   * @param option
   */
  app.prototype.openVideoPlayer = function (option) {

    if ($.isEmptyObject(option)) {
      return;
    }

    var cmd = 'CLIENT_VIDEO_PLAYER';

    var defultOpt = {
      seriesid: 0, // 系列id
      title: '', // 视频标题
      videopathm3u8: '', // m3u8视频播放地址
      videopathmp4: '', // mp4播放地址
      candownload: 0 // 是否允许下载,默认不允许下载  (1可以下载，0不可下载)
    };

    var videos = new Array();

    if ($.isArray(option)) {
      for (var i = 0, o; o = option[i++];) {
        if (!$.isEmptyObject(o)) {
          videos.push($.extend(defultOpt, o));
        }
      }
    } else {
      videos.push($.extend(defultOpt, option));
    }

    B.postNotification(cmd, { videolist: videos });
  };

  /**
   * 启动直播
   * @param data
   */
  app.prototype.openVideoLive = function (data) {
    var cmd = 'CLIENT_OPEN_LIVE';
    B.postNotification(cmd, data);
  };

  /**
   * 打开资源列表
   */
  app.prototype.openUpResource = function () {
    if (this.isChaoxingApp()) {
      var cmd = 'CLIENT_SELECT_OPTION_BAR';
      B.unbind(cmd);
      B.postNotification(cmd, { "show": 1 });
    } else {
      this.noAppMsg();
    }
  }

  /**
   * 绑定图片视频回调
   * @param callback
   */
  app.prototype.binkUpPicVideo = function (callback) {
    var cmd = "CLIENT_CHOOSE_IMAGE_RESULT";
    B.unbind(cmd);
    B.bind(cmd, callback);
  };

  /**
   * 绑定书房回调
   * @param callback
   */
  app.prototype.binkUpRes = function (callback) {
    var cmd = "CLIENT_SELECT_RES";
    B.unbind(cmd);
    B.bind(cmd, callback);
  };

  /**
   * 绑定云盘回调
   * @param callback
   */
  app.prototype.binkUpCloudRes = function (callback) {
    var cmd = "CLIENT_SELECT_CLOUDRES";
    B.unbind(cmd);
    B.bind(cmd, callback);
  };

  /**
   * 绑定笔记回调
   * @param callback
   */
  app.prototype.binkUpNode = function (callback) {
    var cmd = "CLIENT_SELECT_NOTE";
    B.unbind(cmd);
    B.bind(cmd, callback);
  };

  /**
   * 绑定资料库回调
   * @param callback
   */
  app.prototype.binkUpExtrainfo = function (callback) {
    var cmd = "CLIENT_WEB_EXTRAINFO";
    B.unbind(cmd);
    B.bind(cmd, callback);
  };

  W['app'] = new app();
  //一些其他方法
  Array.prototype.remove = function (val) {
    var index = this.indexOf(val);
    if (index > -1) {
      this.splice(index, 1);
    }
  };
})(window, jQuery, JSON, jsBridge);
$(function () {
  try {
    FastClick.attach(document.body);
  } catch (e) {}
});

function _jsBridgeReady() {
  ready();
}
$(document).ready(function () {
  if (!app.isChaoxingApp()) {
    _jsBridgeReady();
  }
});