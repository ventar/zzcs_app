function popOpen() {
  $("#popBg,#popBox").show();
  $("body").css("overflow", "hidden");
  $(window).scroll(function () {
    $(this).scrollTop(0)
  });
  $(document).bind("touchmove", function (e) {
    e.preventDefault();
  });
}

function popClose() {
  $("#popBg,#popBox").hide();
  $("body").css("overflow", "");
  $(window).unbind("scroll");
  $(document).unbind("touchmove");
}